// Hell Top Down Shooter. All right reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class HellTopDownShooterTarget : TargetRules
{
	public HellTopDownShooterTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V4;

		ExtraModuleNames.AddRange( new string[] { "HellTopDownShooter" } );
	}
}

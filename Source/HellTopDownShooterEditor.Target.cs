// Hell Top Down Shooter. All right reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class HellTopDownShooterEditorTarget : TargetRules
{
	public HellTopDownShooterEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V4;

		ExtraModuleNames.AddRange( new string[] { "HellTopDownShooter" } );
	}
}

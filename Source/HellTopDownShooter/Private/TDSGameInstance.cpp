// Hell Top Down Shooter. All right reserved.

#include "TDSGameInstance.h"
#include "FuncLibrary/TDSTypes.h"

DEFINE_LOG_CATEGORY_STATIC(TDSGameInstanceLog, All, All)

bool UTDSGameInstance::GetWeaponDataByName(FName WeaponName, FWeaponData& OutData)
{
    FWeaponData* WeaponInfoRow;

    if (WeaponDataTable)
    {
        WeaponInfoRow = WeaponDataTable->FindRow<FWeaponData>(WeaponName, "", false);
        
        if (WeaponInfoRow)
        {
            OutData = *WeaponInfoRow;
            return true;
        }
    }
    else
    {
        UE_LOG(TDSGameInstanceLog, Warning, TEXT("---GetWeaponInfoByName - WeaponTable -NULL"));
    }
    return false;
}

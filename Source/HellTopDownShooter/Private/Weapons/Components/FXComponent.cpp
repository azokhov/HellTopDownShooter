// Hell Top Down Shooter. All right reserved.

#include "Weapons/Components/FXComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Components/DecalComponent.h"

DEFINE_LOG_CATEGORY_STATIC(TDSFXComponentLog, All, All)

UFXComponent::UFXComponent()
{
    PrimaryComponentTick.bCanEverTick = false;
}

void UFXComponent::PlayImpactFX(const FHitResult& Hit)
{
    FImpactData ImpactData;
    if (ImpactDataMap.Num() > 0)
    {
        for (auto It = ImpactDataMap.CreateIterator(); It; ++It)
        {
            ImpactData = It.Value();
        }
    }
    UE_LOG(TDSFXComponentLog, Display, TEXT("HitDecal: %s"), *ImpactData.DecalData.HitDecal->GetName());

    if (Hit.PhysMaterial.IsValid())
    {
        const auto Surface = UGameplayStatics::GetSurfaceType(Hit);
        if (ImpactDataMap.Contains(Surface))
        {
            ImpactData = ImpactDataMap[Surface];
        }
    }
    else
    {
        UE_LOG(TDSFXComponentLog, Display, TEXT("No PhysMaterial"));
    }

    if (ImpactData.DecalData.HitDecal)
    {
        auto DecalComponent = UGameplayStatics::SpawnDecalAttached( //
            ImpactData.DecalData.HitDecal,                          //
            ImpactData.DecalData.Size,                              //
            Hit.GetComponent(),                                     //
            NAME_None,                                              //
            Hit.ImpactPoint,                                        //
            Hit.ImpactNormal.Rotation(),                            //
            EAttachLocation::KeepWorldPosition,                     //
            ImpactData.DecalData.LifeTime);

        if (DecalComponent)
        {
            DecalComponent->SetFadeOut(ImpactData.DecalData.LifeTime, ImpactData.DecalData.FadeOutTime, false);
        }
    }
    else
    {
        UE_LOG(TDSFXComponentLog, Display, TEXT("No HitDecal"));
    }

    if (ImpactData.HitFX)
    {
        UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactData.HitFX, Hit.ImpactPoint, Hit.ImpactNormal.Rotation());
    }
    else
    {
        UE_LOG(TDSFXComponentLog, Display, TEXT("No HitFX"));
    }

    if (ImpactData.HitSound)
    {
        UGameplayStatics::SpawnSoundAtLocation(GetWorld(), ImpactData.HitSound, Hit.ImpactPoint);
    }
    else
    {
        UE_LOG(TDSFXComponentLog, Display, TEXT("No HitSound"));
    }
}

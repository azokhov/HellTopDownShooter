// Hell Top Down Shooter. All right reserved.

#include "Weapons/WeaponBase.h"
#include "Weapons/Components/FXComponent.h"
#include "Components/ArrowComponent.h"
#include "Weapons/ProjectileBase.h"
#include "TDSGameInstance.h"
#include "GameFramework/Character.h"
#include "GameFramework/Controller.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/GameplayStatics.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Components/SkinnedMeshComponent.h"
#include "DrawDebugHelpers.h"

DEFINE_LOG_CATEGORY_STATIC(TDSWeaponBaseLog, All, All)

AWeaponBase::AWeaponBase()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    SceneWeapon = CreateDefaultSubobject<USceneComponent>(TEXT("SceneWeapon"));
    SetRootComponent(SceneWeapon);

    SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
    SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
    SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
    SkeletalMeshWeapon->SetupAttachment(GetRootComponent());

    StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
    StaticMeshWeapon->SetGenerateOverlapEvents(false);
    StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
    StaticMeshWeapon->SetupAttachment(GetRootComponent());

    ShootVector = CreateDefaultSubobject<UArrowComponent>(TEXT("Shoot Vector"));
    ShootVector->SetupAttachment(GetRootComponent());

    WeaponFXComponent = CreateDefaultSubobject<UFXComponent>("WeaponFXComponent");
}

void AWeaponBase::BeginPlay()
{
    Super::BeginPlay();
    WeaponInit();
}

void AWeaponBase::WeaponInit()
{
    if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
    {
        SkeletalMeshWeapon->DestroyComponent(true);
    }

    if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
    {
        StaticMeshWeapon->DestroyComponent(true);
    }
}

void AWeaponBase::SetWeaponData(FWeaponData WeaponData)
{
    WeaponSettings = WeaponData;
    CurrentAmmoData = WeaponData.AmmoData;
    DispersionData = WeaponData.DispersionData;
    CurrentSpread = WeaponData.DispersionData.BaseSpread;
    if (!WeaponData.bIsFireByProjectile)
    {
        WeaponFXComponent->SetImpactDataMap(WeaponData.ImpactDataMap);
    }
}

bool AWeaponBase::CanFire()
{
    return !bIsReloading && !bIsBlockFire;
}

void AWeaponBase::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

void AWeaponBase::Fire()
{
    if (!CanFire()) return;

    if (!ShootVector)
    {
        UE_LOG(TDSWeaponBaseLog, Warning, TEXT("---ShootVector is InValid!!!---"));
        return;
    }

    int8 ShotNum = GetShotNum();

    FHitResult HitResult;
    FVector TraceStart;
    FVector TraceEnd;

    if (!GetTraceData(HitResult, TraceStart, TraceEnd)) return;

    FRotator SpawnRotation = ShootVector->GetComponentRotation();
    const float MuzzleVectorDistance = 1000.0f;
     DrawDebugLine(GetWorld(), TraceStart, TraceStart + SpawnRotation.Vector() * MuzzleVectorDistance, FColor::Red, false);
    UE_LOG(TDSWeaponBaseLog, Display, TEXT("---Ammo before fire %i---"), CurrentAmmoData.ProjectilesAmound);
    UE_LOG(TDSWeaponBaseLog, Warning, TEXT("---Fire start!!! | ShotNum %i ---"), ShotNum);

    for (auto i = 0; i < ShotNum; ++i)
    {
        if (GetWeaponData().bIsWithDispersion)
        {
            TraceEnd = ApplyDispersion(TraceStart, TraceEnd);

            /* debug dispersion*/
            const auto HalfRad = FMath::DegreesToRadians(GetCurrentSpread() * GetCurrentModifier());
            const FVector ShootDirection = FMath::VRandCone(TraceEnd - TraceStart, HalfRad);
            DrawDebugCone(GetWorld(), TraceStart, ShootDirection, 1000.0f, HalfRad, HalfRad, 24, FColor::Magenta, false);
            /*----------------*/
        }

        if (GetWeaponData().bIsFireByProjectile)
        {
            FireByProjectile(TraceStart, TraceEnd);
        }
        else
        {
            FireByTrace(HitResult, TraceStart, TraceEnd);
        }

        UE_LOG(TDSWeaponBaseLog, Display, TEXT("---Shot %i ---"), i + 1);
    }

    // PlayFX(SpawnLocation, ShootVector->GetComponentTransform());
    if (GetWeaponData().WeaponFire)
    {
        SkeletalMeshWeapon->PlayAnimation(GetWeaponData().WeaponFire, false);
    }

    OnWeaponFireStart.Broadcast();

    DecreaseAmmo(ShotNum);
    IncreaseDispersion();
    EjectSleeve();

    UE_LOG(TDSWeaponBaseLog, Display, TEXT("---Fire complete!!!---"));
}

bool AWeaponBase::GetTraceData(FHitResult& HitResult, FVector& TraceStart, FVector& TraceEnd) const
{
    const auto Character = Cast<ACharacter>(GetOwner());
    if (!Character) return false;

    const auto PC = Character->GetController<APlayerController>();
    if (!PC) return false;

    FHitResult CursorHitResult;
    PC->GetHitResultUnderCursor(ECollisionChannel::ECC_GameTraceChannel1, true, CursorHitResult);
    HitResult = CursorHitResult;

    if (!ShootVector) return false;
    FVector MuzzleLocation = ShootVector->GetComponentLocation();
    TraceStart = MuzzleLocation;

    FVector ShootEndLocation = FVector::Zero();

    float MaxTraceDistance = 1500.0f;
    float MaxAngle = 20.0f; // max angle betwen Shoot direction and Barrel direction

    if (CursorHitResult.bBlockingHit)
    {
        ShootEndLocation = CursorHitResult.Location;
    }
    else
    {
        /* Fire by muzzle plane*/
        FVector MouseLocation, MouseDirection;
        PC->DeprojectMousePositionToWorld(MouseLocation, MouseDirection);

        FVector LineStart = MouseLocation;
        FVector LineDirection = MouseLocation + MouseDirection * MaxTraceDistance;
        FVector PlanePoint = MuzzleLocation;
        FVector PlaneNormal = FVector::UnitZ();

        FVector IntersectionPoint = FMath::LinePlaneIntersection(LineStart, LineDirection, PlanePoint, PlaneNormal);

        ShootEndLocation = IntersectionPoint;
        /* ------------- */
    }

    /* Check angle */
    FVector MuzzleDirection = ShootVector->GetForwardVector();
    FVector ShootDirectionNormal = (ShootEndLocation - MuzzleLocation).GetSafeNormal();
    FVector ShootDirectionByBarrel = MuzzleLocation + MuzzleDirection * MaxTraceDistance;
    const auto AngleBetween = FMath::Acos(FVector::DotProduct(MuzzleDirection, ShootDirectionNormal));
    const float Degrees = FMath::RadiansToDegrees(AngleBetween);

    if (Degrees > MaxAngle)
    {
        UE_LOG(TDSWeaponBaseLog, Warning, TEXT("---Shoot by barrel---"));
        ShootEndLocation = ShootDirectionByBarrel;
    }
    /* --------- */

    TraceEnd = ShootEndLocation;

    return true;
}

void AWeaponBase::FireByProjectile(const FVector& TraceStart, const FVector& TraceEnd)
{

    const auto ProjectileData = GetProjectileData();
    const auto ProjectileClass = ProjectileData.ProjectileClass;

    if (ProjectileClass)
    {
        /**
        FActorSpawnParameters SpawnParams;
        SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
        SpawnParams.Owner = GetOwner();
        SpawnParams.Instigator = GetInstigator();
        /**/

        FVector Direction = TraceEnd - TraceStart;
        FRotator SpawnRotation = FRotationMatrix::MakeFromX(Direction).Rotator();
        const FTransform SpawnTransform(SpawnRotation, TraceStart);

        // auto Actor = GetWorld()->SpawnActor(ProjectileClass, &TraceStart, &SpawnRotation, SpawnParams);
        // auto Projectile = Cast<AProjectileBase>(Actor);

        auto Projectile = GetWorld()->SpawnActorDeferred<AProjectileBase>(ProjectileClass, SpawnTransform, GetOwner(), GetInstigator());

        if (Projectile)
        {
            Projectile->InitProjectile(ProjectileData, GetWeaponData().ImpactDataMap);
            Projectile->SpawnCollisionHandlingMethod = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
            Projectile->FinishSpawning(SpawnTransform);
        }
        else
        {
            UE_LOG(TDSWeaponBaseLog, Display, TEXT("---Projectile null---"));
        }
    }
}

void AWeaponBase::FireByTrace(FHitResult& HitResult, FVector& TraceStart, FVector& TraceEnd)
{

    FCollisionQueryParams CollisionParams;
    CollisionParams.bReturnPhysicalMaterial = true;
    CollisionParams.AddIgnoredActor(GetOwner());
    GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECollisionChannel::ECC_Visibility, CollisionParams);

    if (HitResult.bBlockingHit)
    {
        // damage
        UGameplayStatics::ApplyDamage(             //
            HitResult.GetActor(),                  //
            GetWeaponData().WeaponDamage,          //
            GetOwner()->GetInstigatorController(), //
            GetOwner(),                            //
            UDamageType::StaticClass());

        WeaponFXComponent->PlayImpactFX(HitResult);
    }
    else
    {
        UE_LOG(TDSWeaponBaseLog, Warning, TEXT("No blocking hit detected."));
    }

    auto TraceEndPoint = HitResult.bBlockingHit ? HitResult.ImpactPoint : TraceEnd;
    SpawnTraceFX(TraceStart, TraceEndPoint);
}

void AWeaponBase::FireByBool(bool bPressed)
{
    if (bPressed)
    {
        // start timer
        if (GetWorldTimerManager().IsTimerActive(DispersionTimerHandle))
        {
            GetWorldTimerManager().ClearTimer(DispersionTimerHandle);
        }
        GetWorldTimerManager().SetTimer(FireTimerHandle, this, &AWeaponBase::Fire, GetWeaponData().RateOfFire, true, 0.0f);
    }

    if (!bPressed)
    {
        // stop timer
        GetWorldTimerManager().ClearTimer(FireTimerHandle);
        GetWorldTimerManager().SetTimer(
            DispersionTimerHandle, this, &AWeaponBase::DecreaseDispersion, GetDispersionData().SpreadDecay, true, 0.0f);

        // UE_LOG(LogTemp, Display, TEXT("--- DispersionTimerHandle -=START=- ---"));
    }
}

void AWeaponBase::DecreaseAmmo(int32 DecreaseAmount)
{
    CurrentAmmoData.ProjectilesAmound = CurrentAmmoData.ProjectilesAmound - DecreaseAmount;
    UE_LOG(TDSWeaponBaseLog, Display, TEXT("---Left %i ammo---"), CurrentAmmoData.ProjectilesAmound);
    if (CurrentAmmoData.ProjectilesAmound == 0)
    {
        TryReloadWeapon();
    }
}

void AWeaponBase::TryReloadWeapon()
{
    GetWorldTimerManager().SetTimer(ReloadTimerHandle, this, &AWeaponBase::ReloadWeaponComplete, GetWeaponData().ReloadTime, false);
    bIsReloading = true;

    // if (GetWeaponData().SoundReloadWeapon)
    //{
    //     UGameplayStatics::SpawnSoundAtLocation(GetWorld(), GetWeaponData().SoundReloadWeapon, ShootVector->GetComponentLocation());
    // }
    // else
    //{
    //     UE_LOG(LogTemp, Display, TEXT("---SoundReloadWeapon invalid---"));
    // }
    EjectClip();

    if (GetWeaponData().WeaponReload)
    {
        SkeletalMeshWeapon->PlayAnimation(GetWeaponData().WeaponReload, false);
    }
    else
    {
        UE_LOG(TDSWeaponBaseLog, Display, TEXT("---WeaponReloadAnim invalid---"));
    }
    if (GetWeaponData().CharacterReload)
    {
        OnWeaponReloadStart.Broadcast(GetWeaponData().CharacterReload);

        UE_LOG(TDSWeaponBaseLog, Display, TEXT("---CharacterReloadAnim Broadcast %s---"), *GetWeaponData().CharacterReload->GetName());
    }
    else
    {
        UE_LOG(TDSWeaponBaseLog, Display, TEXT("---CharacterReloadAnim invalid---"));
    }
    UE_LOG(TDSWeaponBaseLog, Display, TEXT("---Reload start---"));
}

void AWeaponBase::ReloadWeaponComplete()
{
    bIsReloading = false;
    SkeletalMeshWeapon->UnHideBoneByName(CurrentAmmoData.ClipBoneName);
    CurrentAmmoData.ProjectilesAmound = GetWeaponData().AmmoData.ProjectilesAmound;
    OnWeaponReloadEnd.Broadcast();
    UE_LOG(TDSWeaponBaseLog, Display, TEXT("---Reload complete---"));
}

FVector AWeaponBase::ApplyDispersion(FVector& ShootStartLocation, FVector& ShootEndLocation)
{
    const auto HalfRad = FMath::DegreesToRadians(GetCurrentSpread() * GetCurrentModifier());
    const FVector ShootDirection = FMath::VRandCone(ShootEndLocation - ShootStartLocation, HalfRad);
    ShootEndLocation = ShootStartLocation + ShootDirection * 1000.0f;

    return ShootEndLocation;
}

float AWeaponBase::GetCurrentSpread()
{
    if (!CurrentSpread)
    {
        return 1.0f;
        UE_LOG(TDSWeaponBaseLog, Warning, TEXT("---Current  Dispersion is Invalid ---"));
    }
    return CurrentSpread;
}

int32 AWeaponBase::GetShotNum() const
{
    return GetWeaponData().ProjectileByOneShot > CurrentAmmoData.ProjectilesAmound //
               ? CurrentAmmoData.ProjectilesAmound
               : GetWeaponData().ProjectileByOneShot;
}

void AWeaponBase::DecreaseDispersion()
{
    if (CurrentSpread <= DispersionData.BaseSpread)
    {
        CurrentSpread = DispersionData.BaseSpread;
        GetWorldTimerManager().ClearTimer(DispersionTimerHandle);
        // UE_LOG(LogTemp, Display, TEXT("--- DispersionTimerHandle STOP ---"));
    }
    else
    {
        CurrentSpread -= DispersionData.SpreadIncrement;
        // UE_LOG(LogTemp, Display, TEXT("--- CurrentDispersion: %f ---"), CurrentSpread);
    }
}

void AWeaponBase::IncreaseDispersion()
{
    if (CurrentSpread >= DispersionData.MaxSpread)
    {
        CurrentSpread = DispersionData.MaxSpread;
    }
    else
    {
        CurrentSpread += DispersionData.SpreadIncrement;
    }
    // UE_LOG(LogTemp, Display, TEXT("--- CurrentDispersion: %f ---"), CurrentSpread);
}

void AWeaponBase::SetCurrentModifier(EMovementState CurrentMovementState)
{
    if (GetDispersionData().MovementStateModifiers.Contains(CurrentMovementState))
    {
        CurrentModifier = GetDispersionData().MovementStateModifiers[CurrentMovementState];
    }

    UE_LOG(TDSWeaponBaseLog, Display, TEXT("---Current Modifier: %f | %s ---"), CurrentModifier,
        *UEnum::GetValueAsString(CurrentMovementState));
}

void AWeaponBase::PlayFX(FVector Location, FTransform Transform)
{
    if (!GetWeaponData().SoundFireWeapon)
    {
        UE_LOG(TDSWeaponBaseLog, Warning, TEXT("--- SoundFireWeapon not setup! Please, setup SoundFireWeapon in Table---"));
        return;
    }
    if (!GetWeaponData().EffectFireWeapon)
    {
        UE_LOG(TDSWeaponBaseLog, Warning, TEXT("--- EffectFireWeapon not setup! Please, setup EffectFireWeapon in Table---"));
        return;
    }
    UGameplayStatics::SpawnSoundAtLocation(GetWorld(), GetWeaponData().SoundFireWeapon, Location);
    UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), GetWeaponData().EffectFireWeapon, Transform);
}

void AWeaponBase::SpawnTraceFX(const FVector& TraceStart, const FVector& TraceEnd)
{
    /**/
    if (!GetWeaponData().NiagaraTraceFX) return;
    const auto TraceFXComponent = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), GetWeaponData().NiagaraTraceFX, TraceStart);

    if (TraceFXComponent)
    {
        TraceFXComponent->SetVariableVec3(GetWeaponData().TraceTargetName, TraceEnd);
        // TraceFXComponent->SetNiagaraVariableVec3(GetWeaponData().TraceTargetName, TraceEnd);
    }

    /**
    if (!GetWeaponData().TraceFX) return;
     const auto TraceFXEmitter = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), GetWeaponData().TraceFX, TraceStart);
     if(TraceFXEmitter)
     {
        TraceFXEmitter->SetVectorParameter(GetWeaponData().TraceTargetName, TraceEnd);
     }
    /**/
}

bool AWeaponBase::EjectActor(
    UStaticMesh* InStaticMesh, FVector SpawnLocation, FRotator SpawnRotation, FVector ImpulseVector, float MeshLifeSpan)
{
    AStaticMeshActor* EjectActor = nullptr;
    FActorSpawnParameters SpawnParams;
    SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
    EjectActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), SpawnLocation, SpawnRotation, SpawnParams);

    if (!EjectActor)
    {
        UE_LOG(TDSWeaponBaseLog, Warning, TEXT("Eject Actor is INVALID!"));
        return false;
    }
    EjectActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
    EjectActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
    EjectActor->GetStaticMeshComponent()->SetMobility(EComponentMobility::Movable);
    EjectActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
    EjectActor->GetStaticMeshComponent()->SetStaticMesh(InStaticMesh);
    EjectActor->GetStaticMeshComponent()->AddImpulse(ImpulseVector, NAME_None, true);
    EjectActor->SetLifeSpan(MeshLifeSpan);

    return true;
}

void AWeaponBase::EjectSleeve()
{
    float ForceImpulse = CurrentAmmoData.ForceImpulse;
    float DegreeRotate = CurrentAmmoData.DegreeRotate;
    FName SleeveSocetName = CurrentAmmoData.SleeveSocetName;
    const float Angle = 7.0f;
    const auto HalfRad = FMath::DegreesToRadians(Angle);

    FVector SpawnLocation = SkeletalMeshWeapon->GetSocketLocation(SleeveSocetName);
    FRotator SpawnRotation = SkeletalMeshWeapon->GetSocketRotation(SleeveSocetName);
    FRotator ImpulseRotation = SpawnRotation.Add(0.0, 0.0, -DegreeRotate);
    FVector ImpulseDirection = FMath::VRandCone(SpawnRotation.RotateVector(FVector::RightVector), HalfRad);
    FVector ImpulseVector = ImpulseDirection * ForceImpulse;
    /**/

    SpawnRotation.Yaw += 90.0f; // Rotate clip for correct spawn

    /*
    const auto RandomAngle = FMath::RandRange(-10.0f, 10.0f);
    SpawnRotation.Yaw += RandomAngle;
    SpawnRotation.Roll += RandomAngle;
    SpawnRotation.Pitch += RandomAngle;
    */

    SpawnRotation.Normalize();
    /**/
    EjectActor(CurrentAmmoData.SleeveDropMesh, SpawnLocation, SpawnRotation, ImpulseVector, CurrentAmmoData.SleeveLifeSpan);

    /* debug *
    FVector EndLocation = SpawnLocation + ImpulseVector;
    DrawDebugLine(GetWorld(), SpawnLocation, EndLocation, FColor::Red, false, 10.0f);
    /**/
}

void AWeaponBase::EjectClip()
{
    FName ClipBoneName = CurrentAmmoData.ClipBoneName;

    SkeletalMeshWeapon->HideBoneByName(ClipBoneName, EPhysBodyOp::PBO_None);

    FVector SpawnLocation = SkeletalMeshWeapon->GetBoneLocation(ClipBoneName);
    FRotator SpawnRotation = SkeletalMeshWeapon->GetBoneQuaternion(ClipBoneName).Rotator();
    FVector ImpulseVector = SpawnRotation.RotateVector(FVector::DownVector);
    /**/
    SpawnRotation.Yaw += 270.0f; // Rotate sleeve for correct spawn
    SpawnRotation.Normalize();
    /**/
    EjectActor(CurrentAmmoData.ClipDropMesh, SpawnLocation, SpawnRotation, ImpulseVector, CurrentAmmoData.ClipLifeSpan);

    /* debug *
    FVector EndLocation = SpawnLocation + ImpulseVector * 500.0f;
    DrawDebugLine(GetWorld(), SpawnLocation, EndLocation, FColor::Green, false, 10.0f);
    /**/
}
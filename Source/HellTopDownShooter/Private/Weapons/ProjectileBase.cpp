// Hell Top Down Shooter. All right reserved.

#include "Weapons/ProjectileBase.h"
#include "Weapons/Components/FXComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"

DEFINE_LOG_CATEGORY_STATIC(TDSProjectileBaseLog, All, All)

AProjectileBase::AProjectileBase()
{
    PrimaryActorTick.bCanEverTick = true;

    ProjectileCollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Collision"));
    ProjectileCollisionComponent->SetSphereRadius(16.f);
    ProjectileCollisionComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_PhysicsBody, ECollisionResponse::ECR_Block);
    ProjectileCollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    ProjectileCollisionComponent->SetNotifyRigidBodyCollision(true);
    ProjectileCollisionComponent->bReturnMaterialOnMove = true;
    SetRootComponent(ProjectileCollisionComponent);

    ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Projectile Mesh"));
    ProjectileMesh->SetupAttachment(GetRootComponent());
    ProjectileMesh->SetCollisionProfileName(TEXT("NoCollision"));
    ProjectileMesh->SetCanEverAffectNavigation(false); // exclude for navigation

    ProjectileFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Projectile trace"));
    ProjectileFX->SetupAttachment(GetRootComponent());

    ProjectileFXComponent = CreateDefaultSubobject<UFXComponent>("ProjectileFXComponent");

    MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement Component"));
    MovementComponent->SetUpdatedComponent(GetRootComponent());
    MovementComponent->InitialSpeed = 3511.f;
    MovementComponent->MaxSpeed = 5111.f;
}

void AProjectileBase::BeginPlay()
{
    Super::BeginPlay();

    if (ProjectileData.bExplodeOnTimer)
    {
        SetGrenadeTimer();
    }

    ProjectileCollisionComponent->OnComponentHit.AddDynamic(this, &AProjectileBase::OnProjectileHit);
}

void AProjectileBase::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

void AProjectileBase::InitProjectile(FProjectileData DataProjectile, TMap<TEnumAsByte<EPhysicalSurface>, FImpactData> DataMapImpact)
{
    UE_LOG(TDSProjectileBaseLog, Display, TEXT("Initializing projectile with InitialSpeed: %f, MaxSpeed: %f"),
        MovementComponent->InitialSpeed, MovementComponent->MaxSpeed);

    if (MovementComponent)
    {
        MovementComponent->InitialSpeed = DataProjectile.InitialSpeed;
        MovementComponent->MaxSpeed = DataProjectile.MaxSpeed;

        UE_LOG(TDSProjectileBaseLog, Display, TEXT("Projectile MovementComponent InitialSpeed: %f, MaxSpeed: %f"),
            MovementComponent->InitialSpeed, MovementComponent->MaxSpeed);
    }

    SetProjectileData(DataProjectile);
    ProjectileFXComponent->SetImpactDataMap(DataMapImpact);

    SetLifeSpan(ProjectileData.ProjectileLifeSpan);
    ProjectileMesh->SetStaticMesh(ProjectileData.Mesh);
    ProjectileFX->SetTemplate(ProjectileData.TraceFX);
    DeltaBounceTime = DataProjectile.DeltaBounceTime;
    LastBounceTime = -DeltaBounceTime;
}

void AProjectileBase::OnExplode()
{
    MovementComponent->StopMovementImmediately();

    // make damage

    UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(), //
        ProjectileData.BaseDamage,                             //
        ProjectileData.MinDamage,                              //
        GetActorLocation(),                                    //
        ProjectileData.InnerRadius,                            //
        ProjectileData.OuterRadius,                            //
        ProjectileData.Falloff,                                //
        UDamageType::StaticClass(),                            //
        {GetOwner()},                                          //
        this,                                                  //
        GetInstigator()->GetController());

    /*debug Radial Damage*/
    float MidRadius = FMath::Lerp(ProjectileData.OuterRadius, ProjectileData.InnerRadius, 0.5f);
    DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileData.InnerRadius, 24, FColor::Red, false, 5.0f);
    DrawDebugSphere(GetWorld(), GetActorLocation(), MidRadius, 24, FColor::Yellow, false, 5.0f);
    DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileData.OuterRadius, 24, FColor::Green, false, 5.0f);
    /*-----*/
    if(ProjectileData.bExplodeOnTimer && GetWorldTimerManager().IsTimerActive(ExploseTimerHandle))
    {
        FHitResult Hit;
        Hit.ImpactPoint = GetActorLocation();
        Hit.ImpactNormal = GetActorUpVector();

        ProjectileFXComponent->PlayImpactFX(Hit);
        GetWorldTimerManager().ClearTimer(ExploseTimerHandle);
    }

    Destroy();
}

void AProjectileBase::OnProjectileHit( //
    UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
    if (ProjectileData.bExplodeOnTimer && GetWorldTimerManager().GetTimerRemaining(ExploseTimerHandle) >= 0.1f)
    {
        bool bCanSound = GetWorld()->GetTimeSeconds() - LastBounceTime >= DeltaBounceTime;
        UE_LOG(TDSProjectileBaseLog, Display, TEXT("DeltaTime: %f "), GetWorld()->GetTimeSeconds() - LastBounceTime);
        if (IsValid(ProjectileData.BounceSound) && bCanSound)
        {
            UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileData.BounceSound, Hit.ImpactPoint, 0.5f);
            LastBounceTime = GetWorld()->GetTimeSeconds();
            UE_LOG(TDSProjectileBaseLog, Display, TEXT("Sound play"));
        }

        return;
    }
    ProjectileFXComponent->PlayImpactFX(Hit);
    OnExplode();
}

void AProjectileBase::SetGrenadeTimer()
{
    GetWorldTimerManager().SetTimer(ExploseTimerHandle, this, &AProjectileBase::OnExplode, ProjectileData.ExplouseTime);
    if (IsValid(ProjectileData.TimerSound))
    {
        float SoundDuration = ProjectileData.TimerSound->GetDuration(); // 2 sec
        float GrenadeTimer = ProjectileData.ExplouseTime;               // 5 sec
        float PlayDelay = GrenadeTimer - SoundDuration;

        UE_LOG(TDSProjectileBaseLog, Display, TEXT("SoundDuration: %f | GrenadeTimer: %f | PlayDelay: %f"), SoundDuration, GrenadeTimer,
            PlayDelay);

        FTimerHandle TimerSoundHandle;
        GetWorldTimerManager().SetTimer(TimerSoundHandle, this, &AProjectileBase::PlayTimerSound, PlayDelay);
    }
}

void AProjectileBase::PlayTimerSound()
{
    UGameplayStatics::SpawnSoundAttached(
        ProjectileData.TimerSound, GetRootComponent(), NAME_None, GetActorLocation(), EAttachLocation::SnapToTarget);
    UE_LOG(TDSProjectileBaseLog, Display, TEXT("PlayTimerSound: %s"), *ProjectileData.TimerSound->GetName());
}

void AProjectileBase::Destroyed()
{

    if (ProjectileFX)
    {
        UE_LOG(TDSProjectileBaseLog, Display, TEXT("-ProjectileFX : %s!-"), *ProjectileFX->GetName());
        ProjectileFX->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
        // ProjectileFX->bAutoManageAttachment
        // ProjectileFX->SetHiddenInGame(false);
        // ProjectileFX->bAutoDestroy = false;
    }
    Super::Destroyed();
}

void AProjectileBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{

    UE_LOG(TDSProjectileBaseLog, Display, TEXT("-EndPlay!-"));

    Super::EndPlay(EndPlayReason);
}

// Hell Top Down Shooter. All right reserved.

#include "Character/TDSCharacter.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include <Kismet/KismetMathLibrary.h>
#include "Weapons/WeaponBase.h"
#include "TDSGameInstance.h"
#include "DrawDebugHelpers.h"

DEFINE_LOG_CATEGORY_STATIC(TDSCharacterLog, All, All)

// Sets default values
ATDSCharacter::ATDSCharacter()
{
    // Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    // Don't rotate character to camera direction
    bUseControllerRotationPitch = false;
    bUseControllerRotationYaw = false;
    bUseControllerRotationRoll = false;

    // Configure character movement
    GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
    GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
    GetCharacterMovement()->bConstrainToPlane = true;
    GetCharacterMovement()->bSnapToPlaneAtStart = true;

    // Create a camera boom...
    CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
    CameraBoom->SetupAttachment(RootComponent);
    CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
    CameraBoom->TargetArmLength = 800.f;
    CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
    CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level
    CameraBoom->bEnableCameraLag = true;
    CameraBoom->CameraLagSpeed = 3.0f;

    // Create a camera...
    TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
    TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
    TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
}

// Called when the game starts or when spawned
void ATDSCharacter::BeginPlay()
{
    Super::BeginPlay();

    // Add Input Mapping Context
    if (APlayerController* PC = Cast<APlayerController>(GetController()))
    {
        // Get the Enhanced Input Local Player Subsystem from the Local Player related to our Player Controller.
        if (UEnhancedInputLocalPlayerSubsystem* Subsystem =
                ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PC->GetLocalPlayer()))
        {
            Subsystem->ClearAllMappings();
            Subsystem->AddMappingContext(CharacterMappingContext, 0);
        }
    }
    InitWeapon(InitWeaponName);
}

// Called every frame
void ATDSCharacter::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    Look(DeltaTime);
}

// Called to bind functionality to input
void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    UEnhancedInputComponent* Input = Cast<UEnhancedInputComponent>(PlayerInputComponent);

    Input->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ATDSCharacter::Move);
    Input->BindAction(MoveAction, ETriggerEvent::Started, this, &ATDSCharacter::Move);
    Input->BindAction(MoveAction, ETriggerEvent::Completed, this, &ATDSCharacter::Move);
    Input->BindAction(ZoomAction, ETriggerEvent::Triggered, this, &ATDSCharacter::Zoom);
    Input->BindAction(AimAction, ETriggerEvent::Started, this, &ATDSCharacter::Aim);
    Input->BindAction(AimAction, ETriggerEvent::Completed, this, &ATDSCharacter::Aim);
    Input->BindAction(WalkAction, ETriggerEvent::Started, this, &ATDSCharacter::Walk);
    Input->BindAction(WalkAction, ETriggerEvent::Completed, this, &ATDSCharacter::Walk);
    Input->BindAction(SprintAction, ETriggerEvent::Started, this, &ATDSCharacter::Sprint);
    Input->BindAction(SprintAction, ETriggerEvent::Completed, this, &ATDSCharacter::Sprint);
    Input->BindAction(FireAction, ETriggerEvent::Started, this, &ATDSCharacter::Attack);
    Input->BindAction(FireAction, ETriggerEvent::Completed, this, &ATDSCharacter::Attack);
    Input->BindAction(ReloadAction, ETriggerEvent::Started, this, &ATDSCharacter::ReloadWeapon);
}

void ATDSCharacter::InitWeapon(FName WeaponName)
{
    auto GameInstance = Cast<UTDSGameInstance>(GetGameInstance());

    if (!GameInstance) return;

    FWeaponData WeaponData;
    if (!GameInstance->GetWeaponDataByName(WeaponName, WeaponData)) return;

    SetWeaponAnimation(WeaponData);

    if (WeaponData.WeaponClass)
    {
        FVector SpawnLocation = GetActorLocation();
        FRotator SpawnRotation = GetActorRotation();
        FActorSpawnParameters SpawnParams;
        SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
        SpawnParams.Owner = GetOwner();
        SpawnParams.Instigator = GetInstigator();

        auto Item = GetWorld()->SpawnActor(WeaponData.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams);
        AWeaponBase* Weapon = Cast<AWeaponBase>(Item);

        if (Weapon)
        {
            FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
            Weapon->AttachToComponent(GetMesh(), Rule, WeaponSocketName);
            Weapon->SetOwner(this);
            SetCurrentWeapon(Weapon, WeaponData);
            Weapon->OnWeaponFireStart.AddDynamic(this, &ATDSCharacter::WeaponFireStart); // change to AddUObject if blueprin don't need
            Weapon->OnWeaponReloadStart.AddDynamic(this, &ATDSCharacter::WeaponReloadStart);
            Weapon->OnWeaponReloadEnd.AddDynamic(this, &ATDSCharacter::WeaponReloadEnd);
        }
    }
}

void ATDSCharacter::SetWeaponAnimation(FWeaponData WeaponData)
{
    WeaponEquipAnimMontage = WeaponData.CharacterEquip;
    WeaponHipAnimMontage = WeaponData.CharacterFire;
    WeaponIronsightsAnimMontage = WeaponData.CharacterFireAim;
    WeaponReloadAnimMontage = WeaponData.CharacterReload;
}

void ATDSCharacter::SetCurrentWeapon(AWeaponBase* Weapon, FWeaponData WeaponData)
{
    Weapon->SetWeaponData(WeaponData);
    CurrentWeapon = Weapon;
}

void ATDSCharacter::Move(const FInputActionValue& InputActionValue)
{
    AddMovementInput(FVector::ForwardVector, InputActionValue.Get<FVector2D>().Y);
    AddMovementInput(FVector::RightVector, InputActionValue.Get<FVector2D>().X);

    if (bIsMove != InputActionValue.Get<bool>())
    {
        bIsMove = InputActionValue.Get<bool>();
        ChangeMovementState();
    }
}

void ATDSCharacter::Look(float DeltaSeconds)
{
    if (MovementState == EMovementState::Sprint_State) return; // Disabling mouse orientation for Sprint state

    APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);
    if (PC)
    {
        // Enabling mouse orientation

        FVector MouseLocation, MouseDirection;
        PC->DeprojectMousePositionToWorld(MouseLocation, MouseDirection);

        /**/
        FVector LineStart = MouseLocation;
        FVector LineDirection = FVector(MouseLocation + MouseDirection * 1000.0f);
        FVector PlanePoint = GetMesh()->GetComponentLocation();
        FVector PlaneNormal = GetActorLocation().UnitZ();

        FVector IntersectionPoint = FMath::LinePlaneIntersection(LineStart, LineDirection, PlanePoint, PlaneNormal);
        /**/

        FRotator NewRotation = UKismetMathLibrary::FindLookAtRotation(PlanePoint, IntersectionPoint);

        SetActorRotation(FRotator(0.0f, NewRotation.Yaw, 0.0f));

        // debug
        // DrawDebugLine(GetWorld(), FVector(IntersectionPoint + FVector(0.0f, 0.0f, 1000.0f)), IntersectionPoint, FColor::Green);
        // DrawDebugSphere(GetWorld(), IntersectionPoint, 1.0f, 8, FColor::Green);
    }
}

void ATDSCharacter::Zoom(const FInputActionValue& InputActionValue)
{
    float CameraHeight = CameraBoom->TargetArmLength;
    float ZoomValue = -InputActionValue.Get<float>() * ZoomStep; // minus for inverted zoom with scroll wheel
    float NewCameraHeight = CameraHeight + ZoomValue;
    float DeltaTime = UGameplayStatics::GetWorldDeltaSeconds(GetWorld());

    NewCameraHeight = UKismetMathLibrary::Clamp(NewCameraHeight, CameraHeightMin, CameraHeightMax);
    CameraBoom->TargetArmLength = UKismetMathLibrary::FInterpTo(CameraHeight, NewCameraHeight, DeltaTime, Smooth);
}

void ATDSCharacter::Aim(const FInputActionValue& InputActionValue)
{
    bIsAim = InputActionValue.Get<bool>();
    ChangeMovementState();
}

void ATDSCharacter::Walk(const FInputActionValue& InputActionValue)
{
    bIsWalk = InputActionValue.Get<bool>();
    ChangeMovementState();
}

void ATDSCharacter::Sprint(const FInputActionValue& InputActionValue)
{
    bWantsToSprint = InputActionValue.Get<bool>();
    ChangeMovementState();
}

void ATDSCharacter::Attack(const FInputActionValue& InputActionValue)
{
    if (GetCurrentWeapon())
    {
        GetCurrentWeapon()->FireByBool(InputActionValue.Get<bool>());
    }
}

void ATDSCharacter::ReloadWeapon(const FInputActionValue& InputActionValue)
{
    if (GetCurrentWeapon())
    {
        GetCurrentWeapon()->TryReloadWeapon();
    }
}

bool ATDSCharacter::IsAiming()
{
    return bIsAim;
}

bool ATDSCharacter::IsWalking()
{
    return bIsWalk || !IsMoving();
}

bool ATDSCharacter::IsSprinting()
{
    return bWantsToSprint && IsMoving() && !IsEnergyZero();
}

void ATDSCharacter::CharacterUpdate()
{
    float ResSpeed = MovementInfo.RunSpeed;
    switch (MovementState)
    {
    case EMovementState::Aim_State:
        ResSpeed = MovementInfo.AimSpeed;
        break;
    case EMovementState::Walk_State:
        ResSpeed = MovementInfo.WalkSpeed;
        break;
    case EMovementState::Run_State:
        ResSpeed = MovementInfo.RunSpeed;
        break;
    case EMovementState::Sprint_State:
        ResSpeed = MovementInfo.SprintSpeed;
        break;
    default:
        break;
    }
    GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDSCharacter::ChangeMovementState()
{
    if (IsAiming())
    {
        MovementState = EMovementState::Aim_State;
    }
    else if (IsWalking())
    {
        MovementState = EMovementState::Walk_State;
    }
    else if (IsSprinting())
    {
        MovementState = EMovementState::Sprint_State;

        // start timer
        if (IsEnergyFull() && !GetWorldTimerManager().IsTimerActive(EnergyChargeTimer))
        {
            GetWorldTimerManager().SetTimer(EnergyChargeTimer, this, &ATDSCharacter::EnergyChange, RateEnergyCharge, true);
            UE_LOG(TDSCharacterLog, Display, TEXT("^^^^^^^^^^^^^ Timer start ^^^^^^^^^^^^^^^"));
        }
    }
    else // Run_State is the default state when any other state is not active
    {
        MovementState = EMovementState::Run_State;
    }

    // check timer
    if (MovementState != EMovementState::Sprint_State && IsEnergyFull() && GetWorldTimerManager().IsTimerActive(EnergyChargeTimer))
    {
        // stop timer
        GetWorldTimerManager().ClearTimer(EnergyChargeTimer);
        UE_LOG(TDSCharacterLog, Display, TEXT("vvvvvvvvvvv Timer Stop vvvvvvvvvvvvv"));
    }
    CharacterUpdate();
    if (GetCurrentWeapon())
    {
        GetCurrentWeapon()->SetCurrentModifier(GetMovementState());
        GetCurrentWeapon()->SetBlockFire(MovementState == EMovementState::Sprint_State);
    }

    /*  Log for MovementState, Velocity and Energy *
    FString MovementStateString = UEnum::GetValueAsString(GetMovementState());
    UE_LOG(LogTemp, Display, TEXT("| %s | Energy: %.2f | Velocity: X:%.2f Y:%.2f Z:%.2f | Move? %s"), //
        *MovementStateString,                                                                         //
        GetEnergy(),                                                                                  //
        GetVelocity().X, GetVelocity().Y, GetVelocity().Z,                                            //
        (IsMoving() ? TEXT("Yes!") : TEXT("No-no-no!")));
    /*-----------------------------------------------------------------------------------------*/
}

void ATDSCharacter::EnergyChange()
{
    if ((MovementState == EMovementState::Sprint_State) && !GetVelocity().IsNearlyZero())
    {
        DecreaseEnegry();
    }
    else
    {
        IncreaseEnegry();
    }

    if (IsEnergyFull() || IsEnergyZero())
    {
        ChangeMovementState();
    }
    UE_LOG(TDSCharacterLog, Display, TEXT("| Energy: %.2f | "), GetEnergy());
}

void ATDSCharacter::DecreaseEnegry()
{
    Energy -= ChargeStep;
    if (Energy < 0.0f)
    {
        Energy = 0.0f;
    }
}

void ATDSCharacter::IncreaseEnegry()
{
    Energy += ChargeStep;
    if (Energy > MaxEnergy)
    {
        Energy = MaxEnergy;
    }
}

void ATDSCharacter::WeaponReloadStart(UAnimMontage* CharacterReloadAnimation)
{
    PlayAnimMontage(CharacterReloadAnimation); // TO DO: change to WeaponReloadAnimMontage Character property
    
    WeaponReloadStart_BP(CharacterReloadAnimation); // if we need blueprint
}
void ATDSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* CharacterReloadAnimation) {/* in BP */}

void ATDSCharacter::WeaponReloadEnd()
{
    WeaponReloadEnd_BP(); // if we need blueprint
}

void ATDSCharacter::WeaponReloadEnd_BP_Implementation() {/* in BP */}

void ATDSCharacter::WeaponFireStart()
{
    if (IsAiming())
    {
        if (!WeaponIronsightsAnimMontage)
        {
            UE_LOG(TDSCharacterLog, Warning, TEXT("CharacterFireAim invalid! Please, setup this property in Table."));
            return;
        }
        PlayAnimMontage(WeaponIronsightsAnimMontage);
    }
    else
    {
        if (!WeaponHipAnimMontage)
        {
            UE_LOG(TDSCharacterLog, Warning, TEXT("CharacterFire invalid! Please, setup this property in Table."));
            return;
        }
        PlayAnimMontage(WeaponHipAnimMontage);
    }
}

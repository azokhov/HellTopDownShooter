// Hell Top Down Shooter. All right reserved.

#include "TDSGameModeBase.h"
#include "Game/TDSPlayerController.h"
#include "Character/TDSCharacter.h"

ATDSGameModeBase::ATDSGameModeBase() 
{
    DefaultPawnClass = ATDSCharacter::StaticClass();
    PlayerControllerClass = ATDSPlayerController::StaticClass();
}

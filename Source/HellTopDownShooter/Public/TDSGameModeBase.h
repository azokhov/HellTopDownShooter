// Hell Top Down Shooter. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDSGameModeBase.generated.h"

UCLASS()
class HELLTOPDOWNSHOOTER_API ATDSGameModeBase : public AGameModeBase
{
    GENERATED_BODY()

public:
    ATDSGameModeBase();
};

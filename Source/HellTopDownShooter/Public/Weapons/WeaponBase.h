// Hell Top Down Shooter. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FuncLibrary/TDSTypes.h"
#include "WeaponBase.generated.h"

class UArrowComponent;
class UFXComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponFireStart);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, AnimReload); // "_DYNAMIC" if we need use this for Bluprint
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponReloadEnd);

UCLASS()
class HELLTOPDOWNSHOOTER_API AWeaponBase : public AActor
{
    GENERATED_BODY()

public:
    AWeaponBase();

    FOnWeaponFireStart OnWeaponFireStart;
    FOnWeaponReloadStart OnWeaponReloadStart;
    FOnWeaponReloadEnd OnWeaponReloadEnd;

protected:
    UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Weapon|Components")
    USceneComponent* SceneWeapon = nullptr;
    UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Weapon|Components")
    USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
    UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Weapon|Components")
    UStaticMeshComponent* StaticMeshWeapon = nullptr;
    UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Weapon|Debug")
    UArrowComponent* ShootVector = nullptr;
    UPROPERTY(VisibleAnywhere, Category = "Weapon|VFX")
    UFXComponent* WeaponFXComponent;
    UPROPERTY()
    FWeaponData WeaponSettings;
    UPROPERTY()
    FAmmoData CurrentAmmoData;
    UPROPERTY()
    FDispersionData DispersionData;

    virtual void BeginPlay() override;

    UFUNCTION()
    void WeaponInit();
    UFUNCTION()
    void Fire();
    UFUNCTION()
    bool EjectActor(UStaticMesh* InStaticMesh, FVector SpawnLocation, FRotator SpawnRotation, FVector ImpulseVector, float MeshLifeSpan);
    UFUNCTION()
    void EjectClip();
    UFUNCTION()
    void EjectSleeve();
    UFUNCTION()
    void DecreaseAmmo(int32 DecreaseAmount);
    UFUNCTION()
    void ReloadWeaponComplete();
    UFUNCTION()
    FVector ApplyDispersion(FVector& ShootStartLocation, FVector& ShootEndLocation);
    UFUNCTION()
    float GetCurrentSpread();
    int32 GetShotNum() const;

    void DecreaseDispersion();
    void IncreaseDispersion();

    bool GetTraceData(FHitResult& HitResult, FVector& TraceStart, FVector& TraceEnd) const;
    void FireByProjectile(const FVector& TraceStart, const FVector& TraceEnd);
    void FireByTrace(FHitResult& HitResult, FVector& TraceStart, FVector& TraceEnd);

private:
    bool bIsReloading = false;
    bool bIsBlockFire = false;
    FTimerHandle FireTimerHandle;
    FTimerHandle ReloadTimerHandle;
    FTimerHandle DispersionTimerHandle;

    float CurrentModifier = 1.0f;
    float CurrentSpread;

    FWeaponData GetWeaponData() const { return WeaponSettings; };
    FProjectileData GetProjectileData() const { return WeaponSettings.ProjectileData; };
    TMap<TEnumAsByte<EPhysicalSurface>, FImpactData> GetImpactDataMap() const { return WeaponSettings.ImpactDataMap; };
    FDispersionData GetDispersionData() const { return DispersionData; };
    float GetCurrentModifier() const { return CurrentModifier; };

    void PlayFX(FVector Location, FTransform Transform);
    void SpawnTraceFX(const FVector& TraceStart, const FVector& TraceEnd);

public:
    virtual void Tick(float DeltaTime) override;

    void FireByBool(bool bPressed);
    bool CanFire();

    void TryReloadWeapon();

    void SetWeaponData(FWeaponData WeaponData);

    void SetCurrentModifier(EMovementState CurrentMovementState);
    void SetBlockFire(bool IsBlock) { bIsBlockFire = IsBlock; };
};

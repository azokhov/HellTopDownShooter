// Hell Top Down Shooter. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FuncLibrary/TDSTypes.h"
#include "FXComponent.generated.h"

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class HELLTOPDOWNSHOOTER_API UFXComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    UFXComponent();

    void SetImpactDataMap(TMap<TEnumAsByte<EPhysicalSurface>, FImpactData> MapDataImpact) { ImpactDataMap = MapDataImpact; };

    void PlayImpactFX(const FHitResult& Hit);

protected:
    TMap<TEnumAsByte<EPhysicalSurface>, FImpactData> ImpactDataMap;
};

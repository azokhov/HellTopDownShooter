// Hell Top Down Shooter. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FuncLibrary/TDSTypes.h"
#include "ProjectileBase.generated.h"

class USphereComponent;
class UProjectileMovementComponent;
class UParticleSystemComponent;
class UFXComponent;

UCLASS()
class HELLTOPDOWNSHOOTER_API AProjectileBase : public AActor
{
    GENERATED_BODY()

public:
    AProjectileBase();

private:
    float LastBounceTime = 0.0f;
    float DeltaBounceTime = 0.1f;

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile|Components")
    USphereComponent* ProjectileCollisionComponent = nullptr;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile")
    UProjectileMovementComponent* MovementComponent = nullptr;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile|Components")
    UStaticMeshComponent* ProjectileMesh = nullptr;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile|FX")
    UParticleSystemComponent* ProjectileFX = nullptr;
    UPROPERTY(VisibleAnywhere, Category = "Projectile|VFX")
    UFXComponent* ProjectileFXComponent;

    FProjectileData ProjectileData;

    FTimerHandle ExploseTimerHandle;

    virtual void BeginPlay() override;

    void SetProjectileData(FProjectileData Data) { ProjectileData = Data; };

    UFUNCTION()
    void OnExplode();
    UFUNCTION()
    void SetGrenadeTimer();
    UFUNCTION()
    void PlayTimerSound();

public:
    virtual void Tick(float DeltaTime) override;
    virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
    virtual void Destroyed() override;
    void InitProjectile(FProjectileData DataProjectile, TMap<TEnumAsByte<EPhysicalSurface>, FImpactData> DataMapImpact);
    UFUNCTION()
    void OnProjectileHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse,
        const FHitResult& Hit);
};

// Hell Top Down Shooter. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TDSGameInstance.generated.h"

struct FWeaponData;

UCLASS()
class HELLTOPDOWNSHOOTER_API UTDSGameInstance : public UGameInstance
{
    GENERATED_BODY()
public:
    UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Weapon Settings")
    UDataTable* WeaponDataTable = nullptr;

    UFUNCTION(BlueprintCallable)
    bool GetWeaponDataByName(FName WeaponName, FWeaponData& OutData);
};

// Hell Top Down Shooter. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FuncLibrary/TDSTypes.h"
#include "TDSCharacter.generated.h"

class UCameraComponent;
class USpringArmComponent;
class UInputMappingContext;
class UInputAction;
class UAnimMontage;
class AWeaponBase;

UCLASS()
class HELLTOPDOWNSHOOTER_API ATDSCharacter : public ACharacter
{
    GENERATED_BODY()

public:
    // Sets default values for this character's properties
    ATDSCharacter();

    UFUNCTION()
    void WeaponReloadStart(UAnimMontage* CharacterReloadAnimation);
    UFUNCTION(BlueprintNativeEvent)
    void WeaponReloadStart_BP(UAnimMontage* CharacterReloadAnimation);
    UFUNCTION()
    void WeaponReloadEnd();
    UFUNCTION(BlueprintNativeEvent)
    void WeaponReloadEnd_BP();
    UFUNCTION(BlueprintCallable)
    void WeaponFireStart();

protected:
    /** Top down camera */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
    UCameraComponent* TopDownCameraComponent;
    /** Camera boom positioning the camera above the character */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
    USpringArmComponent* CameraBoom;
    /** MappingContext */
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
    UInputMappingContext* CharacterMappingContext;
    /** Move Input Action */
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
    UInputAction* MoveAction;
    /** Aim Input Action */
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
    UInputAction* AimAction;
    /** Walk Input Action */
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
    UInputAction* WalkAction; // no Alt because Alt+S it's "Start simulating the game" in Play World by defaults
    /** Sprint Input Action */
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
    UInputAction* SprintAction;
    /** Zoom Input Action */
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
    UInputAction* ZoomAction;
    /** Fire Input Action */
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
    UInputAction* FireAction;
    /** Reload Input Action */
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
    UInputAction* ReloadAction;

    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "TDSCharacter|Movement")
    EMovementState MovementState = EMovementState::Run_State;
    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "TDSCharacter|Movement")
    FCharacterSpeed MovementInfo;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TDSCharacter|Camera")
    float ZoomStep = 200.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TDSCharacter|Camera")
    float Smooth = 10.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TDSCharacter|Camera")
    float CameraHeightMax = 1100.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TDSCharacter|Camera")
    float CameraHeightMin = 700.0f;

    UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "TDSCharacter|Movement")
    bool bIsAim = false;
    UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "TDSCharacter|Movement")
    bool bIsWalk = false;
    UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "TDSCharacter|Movement")
    bool bWantsToSprint = false;
    bool bIsMove = false;

    FTimerHandle EnergyChargeTimer;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TDSCharacter|Energy")
    float RateEnergyCharge = 1.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TDSCharacter|Energy")
    float MaxEnergy = 1.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TDSCharacter|Energy")
    float Energy = MaxEnergy;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TDSCharacter|Energy")
    float ChargeStep = 0.1f;

    UPROPERTY()
    UAnimMontage* WeaponEquipAnimMontage;
    UPROPERTY()
    UAnimMontage* WeaponHipAnimMontage;
    UPROPERTY()
    UAnimMontage* WeaponIronsightsAnimMontage;
    UPROPERTY()
    UAnimMontage* WeaponReloadAnimMontage;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TDSDemo|Weapon")
    FName WeaponSocketName = "Set socket name";
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TDSDemo|Weapon")
    FName InitWeaponName = "Set Init Weapon name";

    AWeaponBase* CurrentWeapon = nullptr;

    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

    UFUNCTION()
    void Move(const FInputActionValue& InputActionValue);
    UFUNCTION()
    void Look(float DeltaSeconds);
    UFUNCTION()
    void Zoom(const FInputActionValue& InputActionValue);
    UFUNCTION()
    void Aim(const FInputActionValue& InputActionValue);
    UFUNCTION()
    void Walk(const FInputActionValue& InputActionValue);
    UFUNCTION()
    void Sprint(const FInputActionValue& InputActionValue);
    UFUNCTION()
    void Attack(const FInputActionValue& InputActionValue);
    UFUNCTION()
    void ReloadWeapon(const FInputActionValue& InputActionValue);

    UFUNCTION(BlueprintCallable)
    bool IsAiming();
    UFUNCTION()
    bool IsWalking();
    UFUNCTION(BlueprintCallable)
    bool IsSprinting();

    UFUNCTION(BlueprintCallable)
    void CharacterUpdate();
    UFUNCTION(BlueprintCallable)
    void ChangeMovementState();
    UFUNCTION()
    void SetWeaponAnimation(FWeaponData WeaponData);
    UFUNCTION(BlueprintCallable)
    void EnergyChange();

    void DecreaseEnegry();
    void IncreaseEnegry();
    bool IsEnergyFull() const { return GetEnergy() == MaxEnergy; };
    bool IsEnergyZero() const { return FMath::IsNearlyZero(GetEnergy()); };

public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;

    // Called to bind functionality to input
    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    UFUNCTION(BlueprintCallable)
    float GetEnergy() const { return Energy; };

    UFUNCTION(BlueprintCallable)
    bool IsMoving() { return bIsMove; };

    UFUNCTION(BlueprintCallable)
    EMovementState GetMovementState() { return MovementState; };

    UFUNCTION(BlueprintCallable)
    void InitWeapon(FName WeaponName);
    UFUNCTION(BlueprintCallable)
    AWeaponBase* GetCurrentWeapon() { return CurrentWeapon; };

    UFUNCTION(BlueprintCallable)
    void SetCurrentWeapon(AWeaponBase* Weapon, FWeaponData WeaponData);

    void PlayAttackAnim(){};
};

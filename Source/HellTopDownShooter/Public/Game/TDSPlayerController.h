// Hell Top Down Shooter. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TDSPlayerController.generated.h"

UCLASS()
class HELLTOPDOWNSHOOTER_API ATDSPlayerController : public APlayerController
{
    GENERATED_BODY()

public:
    ATDSPlayerController();
};

// Hell Top Down Shooter. All right reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "TDSTypes.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
    Aim_State UMETA(DisplayName = "Aim State"),
    Walk_State UMETA(DisplayName = "Walk State"),
    Run_State UMETA(DisplayName = "Run State"),
    Sprint_State UMETA(DisplayName = "Sprint State"),
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
    GENERATED_BODY()

    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
    float AimSpeed = 211.0f;
    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
    float WalkSpeed = 319.3f;
    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
    float RunSpeed = 412.1f;
    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
    float SprintSpeed = 821.4f;
};

class AProjectileBase;
class AWeaponBase;

USTRUCT(BlueprintType)
struct FProjectileData
{
    GENERATED_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ProjectileSettings")
    TSubclassOf<AProjectileBase> ProjectileClass;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile|Components")
    UStaticMesh* Mesh;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile|Components")
    UParticleSystem* TraceFX;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile|Components")
    UParticleSystem* ExplosionFX;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ProjectileSettings", meta = (ClampMin = "0.0"))
    float InitialSpeed = 1000.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ProjectileSettings", meta = (ClampMin = "0.0"))
    float MaxSpeed = 2000.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ProjectileSettings", meta = (ClampMin = "0.0", ClampMax = "1000.0"))
    float ProjectileLifeSpan = 10.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Damage")
    float BaseDamage = 20.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Damage")
    float MinDamage = 2.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Damage", meta = (ClampMin = "0.0"))
    float InnerRadius = 100.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Damage", meta = (ClampMin = "0.0"))
    float OuterRadius = 500.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Damage", meta = (ClampMin = "0.0"))
    float Falloff = 2.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ProjectileSettings")
    bool bExplodeOnTimer = false;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (EditCondition = "bExplodeOnTimer", ClampMin = "0.0"), //
        Category = "Grenade")
    float DeltaBounceTime = 0.1f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (EditCondition = "bExplodeOnTimer", ClampMin = "0.0"),
        Category = "Grenade")
    float ExplouseTime = 10.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Grenade", meta = (EditCondition = "bExplodeOnTimer"))
    USoundBase* TimerSound;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Grenade", meta = (EditCondition = "bExplodeOnTimer"))
    USoundBase* BounceSound;
};

USTRUCT(BlueprintType)
struct FDecalData
{
    GENERATED_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ImpactSettings")
    UMaterialInterface* HitDecal;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ImpactSettings")
    FVector Size = FVector(20.0f);
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ImpactSettings")
    float LifeTime = 5.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ImpactSettings")
    float FadeOutTime = 1.0f;
};

USTRUCT(BlueprintType)
struct FImpactData
{
    GENERATED_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ImpactSettings")
    FDecalData DecalData;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ImpactSettings")
    USoundBase* HitSound = nullptr;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ImpactSettings")
    UParticleSystem* HitFX;
};

USTRUCT(BlueprintType)
struct FAmmoData
{
    GENERATED_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AmmoSettings", meta = (ClampMin = "0", ClampMax = "100"))
    int32 ClipsAmound = 2;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AmmoSettings")
    UStaticMesh* ClipDropMesh = nullptr;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AmmoSettings", meta = (ClampMin = "0.0"))
    float ClipLifeSpan = 5.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AmmoSettings", meta = (ClampMin = "0", ClampMax = "100"))
    int32 ProjectilesAmound = 10;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AmmoSettings")
    UStaticMesh* SleeveDropMesh = nullptr;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AmmoSettings", meta = (ClampMin = "0.0"))
    float SleeveLifeSpan = 5.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AmmoSettings", meta = (ClampMin = "0.0"))
    float ForceImpulse = 200.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AmmoSettings", meta = (ClampMin = "-90.0", ClampMax = "90.0"))
    float DegreeRotate = 45.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AmmoSettings")
    FName SleeveSocetName = "AmmoEject";
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AmmoSettings")
    FName ClipBoneName = "Clip_Bone";
};

USTRUCT(BlueprintType)
struct FDispersionData
{
    GENERATED_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DispersionSettings", meta = (ClampMin = "0.0", ClampMax = "10.0"))
    float BaseSpread = 1.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DispersionSettings", meta = (ClampMin = "0.0", ClampMax = "10.0"))
    float SpreadIncrement = 0.5f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DispersionSettings", meta = (ClampMin = "0.0", ClampMax = "10.0"))
    float SpreadDecay = 1.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DispersionSettings", meta = (ClampMin = "0.0", ClampMax = "10.0"))
    float MaxSpread = 5.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DispersionSettings", meta = (ClampMin = "0.0", ClampMax = "3.0"))
    TMap<EMovementState, float> MovementStateModifiers;
};

class UNiagaraSystem;

USTRUCT(BlueprintType)
struct FWeaponData : public FTableRowBase
{
    GENERATED_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSettings")
    FText DisplayName;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSettings")
    TSubclassOf<AWeaponBase> WeaponClass;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSettings", meta = (ClampMin = "0.0"))
    float RateOfFire = 0.5f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSettings", meta = (ClampMin = "0.0"))
    float ReloadTime = 2.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSettings", meta = (ClampMin = "1", ClampMax = "100"))
    int32 ProjectileByOneShot = 1;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite /*, Category = "WeaponSettings"*/)
    FAmmoData AmmoData;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSettings", DisplayName = "Fire by projectile")
    bool bIsFireByProjectile = true;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSettings",
        meta = (EditCondition = "!bIsFireByProjectile", ClampMin = "0.0"))
    float WeaponDamage = 20.0f; // for damage by weapon, not by projectile
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite /*, Category = "WeaponSettings"*/, meta = (EditCondition = "bIsFireByProjectile"))
    FProjectileData ProjectileData;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSettings", DisplayName = "Fire with dispersion")
    bool bIsWithDispersion = true;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite /*, Category = "WeaponSettings"*/, meta = (EditCondition = "bIsWithDispersion"))
    FDispersionData DispersionData;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponFX")
    USoundBase* SoundFireWeapon;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponFX")
    USoundBase* SoundReloadWeapon;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponFX")
    UParticleSystem* EffectFireWeapon;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponFX", meta = (EditCondition = "!bIsFireByProjectile"))
    UParticleSystem* TraceFX;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponFX", meta = (EditCondition = "!bIsFireByProjectile"))
    UNiagaraSystem* NiagaraTraceFX;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponFX", meta = (EditCondition = "!bIsFireByProjectile"))
    FName TraceTargetName;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Impact")
    TMap<TEnumAsByte<EPhysicalSurface>, FImpactData> ImpactDataMap;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animation|Weapon")
    UAnimationAsset* WeaponReload;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animation|Weapon")
    UAnimationAsset* WeaponFire;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animation|Character")
    UAnimMontage* CharacterEquip;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animation|Character")
    UAnimMontage* CharacterReload;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animation|Character")
    UAnimMontage* CharacterFire;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animation|Character")
    UAnimMontage* CharacterFireAim;
};

UCLASS()
class HELLTOPDOWNSHOOTER_API UTDSTypes : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()
};
